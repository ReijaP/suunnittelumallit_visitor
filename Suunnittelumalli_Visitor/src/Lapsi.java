import java.util.Random;

public class Lapsi extends HahmoState{
	
	public int bonus = 0;
	private Hahmo hahmo;
	
	public Lapsi(Hahmo hahmo) {
		introduce();
		this.hahmo = hahmo;
	}
	
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	
	public void annaBonus(int n) {
		//Random rand = new Random();
		//int n = rand.nextInt(10);
		this.bonus += n;
		
		if(this.bonus >= 10) {
			System.out.println("Bonusta tuli " + n + " pistettä");
			hahmo.changeState(new Aikuinen(this, hahmo));
		}else {
			System.out.println("Bonusta tuli " + n + " pistettä");
		}
	}
	
	public void introduce() {
		System.out.println("Hahmo on lapsi");
	}
	
	public int getBonus() {
		return this.bonus;
	}

}
