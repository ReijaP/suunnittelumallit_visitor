import java.util.Random;

public class Aikuinen extends HahmoState{
	
	public int bonus;
	private Hahmo hahmo;
	
	public Aikuinen(Lapsi lapsi, Hahmo hahmo) {
		introduce();
		this.bonus = lapsi.getBonus();
		this.hahmo = hahmo;
	}
	
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	
	public void annaBonus(int n) {
		this.bonus += n;
		
		if(this.bonus >= 15) {
			System.out.println("Bonusta tuli " + n + " pistettä");
			hahmo.changeState(new Vanhus(this, hahmo));
		}else {
			System.out.println("Bonusta tuli " + n + " pistettä");
		}
	}
	
	public void introduce() {
		System.out.println("Hahmo on aikuinen");
	}
	
	public int getBonus() {
		return this.bonus;
	}

}
