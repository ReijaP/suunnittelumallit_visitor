import java.util.Random;

public class Vanhus extends HahmoState {
	
	public int bonus;
	private Hahmo hahmo;
	
	public Vanhus(Aikuinen aikuinen, Hahmo hahmo) {
		introduce();
		this.bonus = aikuinen.getBonus();
		this.hahmo = hahmo;
	}
	
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	
	public void annaBonus(int n) {
		this.bonus += n;
		
		if(this.bonus >= 20) {
			System.out.println("Bonusta tuli " + n + " pistettä");
		}else {
			System.out.println("Bonusta tuli " + n + " pistettä");
		}
	}
	
	public void introduce() {
		System.out.println("Hahmo on vanhus");
	}
	
	public int getBonus() {
		return this.bonus;
	}

}
