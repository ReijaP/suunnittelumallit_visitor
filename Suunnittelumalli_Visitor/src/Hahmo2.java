
public class Hahmo2 extends Hahmo{
	
	private HahmoState state;
	
	public Hahmo2() {
		this.state = new Lapsi(this);
	}

	@Override
	public void changeState(HahmoState hs) {
		this.state = hs;
		
	}

	@Override
	public void annaBonus() {
		state.annaBonus();
		
	}

	public void accept(Visitor visitor) {
		state.accept(visitor);
		
	}
	
	public void introduce() {
		state.introduce();
	}
	
	public int getBonus() {
		return state.getBonus();
	}

}
