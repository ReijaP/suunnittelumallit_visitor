import java.util.Random;

public class BonusCountVisitor implements Visitor {
	
	private int bonustotal = 0;
	
	public int getTotal() {
		return this.bonustotal;
	}

	@Override
	public void visit(Aikuinen aikuinen) {
		Random rand = new Random();
		int n = rand.nextInt(10);
		
		aikuinen.annaBonus(n);
		this.bonustotal += aikuinen.getBonus();
		
	}

	@Override
	public void visit(Lapsi lapsi) {
		Random rand = new Random();
		int n = rand.nextInt(10);
		
		lapsi.annaBonus(n);
		this.bonustotal = lapsi.getBonus();
		
	}

	@Override
	public void visit(Vanhus vanhus) {
		Random rand = new Random();
		int n = rand.nextInt(10);
		
		vanhus.annaBonus(n);
		this.bonustotal = vanhus.getBonus();
		
	}

}
