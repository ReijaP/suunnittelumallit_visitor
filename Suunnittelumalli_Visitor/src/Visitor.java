
public interface Visitor {
	
	void visit(Aikuinen aikuinen);
	void visit(Lapsi lapsi);
	void visit(Vanhus vanhus);
	public int getTotal();

}
