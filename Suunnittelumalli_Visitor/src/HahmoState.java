
public abstract class HahmoState {
	
	public void changeState(Hahmo h, HahmoState hs) {
		h.changeState(hs);
	}
	
	public void accept(Visitor visitor) {}
	
	public void annaBonus() {}
	
	public void introduce() {}
	
	public int getBonus() {
		return 0;
	}

}
